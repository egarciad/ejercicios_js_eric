function cambia(){
    console.log("cambia activada");
    let cuadrado = document.getElementById("este");
    cuadrado.style.backgroundColor="blue";
}

// Cambiar un valor html por otro
function substituir(){
    document.getElementById("modificar").innerHTML = "El texto ha cambiado!!";
}

function cambiarCSS(){
    document.getElementById("cambCSS").style.fontSize = "35px";
}

function desaparecer(){
    document.getElementById("invisible").style.display = "none";
}

var coche = {
    nombre: "seat",
    modelo: "leon",
    color: "rojo",
    caracteristica: "FR",
    motor: "150cv",
    fullName: function(){
       return this.nombre + " "+ this.modelo + " "+this.caracteristica + " "+ this.motor + " " + this.color;
    }
}

function Coche(){
    document.getElementById("modelo").innerHTML=coche.nombre + " "+ coche.modelo + " "+coche.caracteristica + " "+ coche.motor + " " + coche.color;
}

$(document).on("click","#boton1", cambiaColores )

function cambiaColores(){
    $(".cuadrado").addClass("verde");
}

$(document).on("mouseenter", ".cuadrado", function(){
    $(".cuadrado").addClass("encima");
})

$(document).on("mouseleave", ".cuadrado", function(){
    $(".cuadrado").removeClass("encima");
})// cambiar .cuadrado por this para que solo afecte a 1