import React from "react";
import {Gato} from "./Gato";
import {Gato300} from "./Gato";
import Cabecera from "./Cabecera";
import Contenido from "./Contenido";
import {Logo} from "./Contenido";
import Luz from "./Luz";
import Padre from "./Padre";
import Taza from "./Taza";
import "./css/app.css";
// import Pie from "./Pie";
import Capital from "./Capital";
import Bootstrap from "./Bootstrap";
import Perro from "./ArrayReact";



function App(){ 

  //let titulo = "mi super app";
  return(  
    <>
       {/* <h1>Hola que tal!!!!!</h1>
       <h1>Welcome to React Parcel Micro App!</h1>
      <p>Hard to get more minimal than this React app.</p>
      <Gato />
      <Gato300 />
        <Cabecera titulo={titulo} color="red"/>
         <Contenido />
         <Logo />
         <Luz />
         <Padre />
         <Taza ancho="200" alto="200" nombre="Taza Star Wars"/>
          */}
         <Capital nom="barcelona" color="red"/>
         <Bootstrap />
         <Perro nom="Jack"/>
    </>
  );
}

export default App;
/*
export default () => (
  <>
    <h1>Hola que tal!!!!</h1>
    <h1>Welcome to React Parcel Micro App!</h1>
    <p>Hard to get more minimal than this React app.</p>
  </>
);
*/