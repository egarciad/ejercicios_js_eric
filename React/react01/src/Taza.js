import React from 'react';
import "./css/taza.css";


class Taza extends React.Component {

    constructor(props){
        super(props);

        this.state={
            fondo: "beige"
        }

        this.cambiaFondo = this.cambiaFondo.bind(this);
    }

    cambiaFondo(){
        let nuevoState= {
            fondo: "red"
        }
        this.setState(nuevoState);
    }

    render() {
        let urlTaza = "https://images-na.ssl-images-amazon.com/images/I/71mpF6fCzlL._AC_SL1500_.jpg";
        let nombreIMG = this.props.nombre;

        let estilo = {
            backgroundColor: this.state.fondo
        };

        return(
            <>
            <div className="caja-taza" style={estilo}>
    
                <img src={urlTaza} alt="Taza" title={nombreIMG}/>
                <p>{this.props.nombre}</p>
            </div>
            <br/>
            <button onClick={this.cambiaFondo}>Cambiar Fondo</button>
            </>
        );
    }
}


export default Taza;


// function Taza(props){

//     let urlTaza = "https://images-na.ssl-images-amazon.com/images/I/71mpF6fCzlL._AC_SL1500_.jpg";
//     let nombreIMG = props.nombre;

//     return(
//         <div className="caja-taza">

//             <img src={urlTaza} alt="Taza" title={nombreIMG}/>
//             <p>{props.nombre}</p>
//         </div>
//     );

// }

