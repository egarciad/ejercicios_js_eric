import React from 'react';
import "./css/cabecera.css";

function Cabecera(props){

    return(
        <div className="cabecera" style={{backgroundColor: props.color}} >{props.titulo}</div>

    );
}


export default Cabecera;