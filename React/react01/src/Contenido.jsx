import React from 'react';

export const Logo = () => <h1> Flecha </h1>;

function TituloRojo(props){
    return(

        <h1 style={{color: "red"}}>{props.children}</h1>
    );
}


function Contenido(props){

    return(
        <>
            <TituloRojo>Capítulo 1</TituloRojo>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Non ducimus explicabo autem voluptatibus aliquid nemo nobis placeat eum itaque. Iste accusantium ratione aut, unde quo placeat obcaecati tenetur repellendus expedita?</p>


                <h1>Capítulo 2</h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Non ducimus explicabo autem voluptatibus aliquid nemo nobis placeat eum itaque. Iste accusantium ratione aut, unde quo placeat obcaecati tenetur repellendus expedita?</p>


        </>
    );
}

export default Contenido;

