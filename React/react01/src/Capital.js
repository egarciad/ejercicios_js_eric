import React from 'react';

class Capital extends React.Component {

    render(){

        //let inicial = this.props.nom[0].toUpperCase(); también válido
        let inicial = this.props.nom[0];
        inicial = inicial.toUpperCase();

        let ciudad = this.props.nom;
        ciudad = inicial + ciudad.substring(1);
        let estilo = {
            color:this.props.color
        };


        return(
            <div className="capital">
                <h1 style={estilo}>{inicial}</h1>
                <p>{ciudad}</p>
            </div>
        );
    }
}

export default Capital;