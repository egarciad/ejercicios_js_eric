import React from "react";




class Perro extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            fondo: "red"
            
        }

        this.cambiaFondo = this.cambiaFondo.bind(this);
    }

    cambiaFondo(){
        let nuevoState= {
            fondo: "green"
        }
        this.setState(nuevoState);
    }

    render() {

        let estilo = {
            backgroundColor: this.state.fondo
        };


        return(
            <>
                <div style={estilo}> 
                    
                    <p>Hola</p>
                    <p>{this.props.nom}</p>
                
                
                </div>

                <button onClick={this.cambiaFondo}>Cambiar fondo</button>
            </>
    
        );
    }
}

export default Perro;