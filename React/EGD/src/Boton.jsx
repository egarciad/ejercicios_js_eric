import React from 'react';
import {Button} from 'reactstrap';

function Boton(props){
                                                // poniendo en el onClick={()=> un arrow function hace que sólo se inicie la función cuando cliquemos y no al cargar la página}
    return(
        <Button block color="warning" onClick={()=>props.clicar(props.texto)}> 
            {props.texto}
        </Button>
    );

}

export default Boton;