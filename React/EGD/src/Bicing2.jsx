import React from 'react';
import { Button, Table, Container, Input, Row, Col } from 'reactstrap';


const API_URL = "https://api.citybik.es/v2/networks/bicing";


class Bicing2 extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            estaciones:[],
            minBicis:0, // valor para filtro de "Capacidad" que le damos al input para que empiece en 0
        }
        this.handleInputChange = this.handleInputChange.bind(this); // bind para el filtro de capacidad de bicis
        this.carga = this.carga.bind(this);
    }
    
    handleInputChange(evento) { // este método nos hace falta para filtrar por "capacidad" de bicis
        const target = evento.target;
        //const value = (target.type === 'checkbox') ? target.checked : target.value;
        const value = target.value;
        const name = target.name;
        this.setState({
          [name]: value
        });
      }
    
    carga() {
        fetch(API_URL)
            .then(response => response.json())
            .then(data => data.network.stations)
            .then(estaciones => this.setState({estaciones: estaciones })) // => console.log(estaciones[0].name))
            .catch(error => console.log(error));
        
        console.log("Fetch iniciado");
     
    }

    render() {

       // let lista = this.state.estaciones.map(el => <li>{el.name}</li>); muestra la lista de todos los elementos que hay en este caso de "name" es como un foreach

       let estacionesFiltradas = this.state.estaciones
                    .filter(estacion => estacion.empty_slots>=this.state.minBicis) // mirará que para cada empty_slots "Capacidad" sea mayor al minBicis que le digamos en el filtro
                    .sort((a,b) => (a.empty_slots > b.empty_slots) ? -1 : 1); // sort siempre espera un 1 o un -1 si no lo ponemos se lia y no funciona de mayor a menos es -1 : 1 y al reves 1 : -1

        let filas =estacionesFiltradas.map((estacion, index) => (
            <tr key={index}>
                <td>{estacion.name}</td>
                <td>{estacion.longitude}</td>
                <td>{estacion.latitude}</td>
                <td>{estacion.empty_slots}</td>
            </tr>
        )

        );
        return (
            <>
                <Container>
                    <br />
                    <h2>Bicing</h2>
                    <br />
                    <Button onClick={this.carga} >Carga Bicis</Button>
                    <Row>
                        <Col >
                            <Input onChange={this.handleInputChange} type="range" min="0" max="50" name="minBicis" value={this.state.minBicis}/> 
                        </Col>
                        <Col>
                            <h3>Capacidad mínima: {this.state.minBicis} <br/>
                            Num estaciones: {estacionesFiltradas.length}</h3>
                        </Col>
                    </Row>
                    <Table>
                        <thead>
                            <tr>
                                <th>Dirección</th>
                                <th>Longitud</th>
                                <th>Latitud</th>
                                <th>Capacidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filas}
                        </tbody>

                    </Table>


                </Container>
            </>
        );
    }

}



export default Bicing2;