import React from 'react';

import Paginacion from "./Paginacion";
import Pantalla from "./Pantalla"; 
import Boton from "./Boton"; 

import { Container, Row, Col } from 'reactstrap';


class Contact extends React.Component{

    constructor(props) {
      super(props);
  
      this.state = {
        textoPantalla: "Selecciona" // valor inicializado, después se cambiará dinámicamente
      }
  
      this.cambia = this.cambia.bind(this);
    }
  
    cambia(nuevoTexto){
      this.setState({textoPantalla: nuevoTexto});
    }
  
    render() {
  
      return (
  
        <>
        
        <Container>
        <Row>
          <Col>
          <Paginacion/>
          </Col>
        </Row>

        <Row>
          <Col>
          <Pantalla texto={this.state.textoPantalla}/>
          </Col>
        </Row>
        <Row>
          <Col>
            <Boton texto="Bici" clicar={this.cambia}/>
          </Col>
          <Col>
            <Boton texto="Moto" clicar={this.cambia}/>
          </Col>
          <Col>
            <Boton texto="Coche" clicar={this.cambia}/>
          </Col>
        </Row>
      </Container>
  
  
        
        </>
      );
  
  
    }
  
  }
  
  export default Contact;