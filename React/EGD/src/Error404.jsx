import React from 'react';
import { Container, Alert } from 'reactstrap';

export default function Error404(){

    return (
        <Container>
        <Alert color="danger" className='mt-5' >
        <h1>Error 404: Página no existe</h1>
        </Alert>
        </Container>
    )
}