import React from 'react';
import Producto from "./Producto";
import { Button, Table, Input } from 'reactstrap';


class Pantalla1 extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            productos: Producto.getProductos(),
            nombreProducto: ""
        }
        this.productoNuevo = this.productoNuevo.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.borra = this.borra.bind(this);
    }


  handleInputChange(evento) {
    const target = evento.target;
    //const value = (target.type === 'checkbox') ? target.checked : target.value;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }


    productoNuevo(){
        Producto.addProducto(this.state.nombreProducto);
        this.setState({productos: Producto.getProductos()})
    }
 
    borra(id){
        Producto.eliminaProducto(id*1);
        this.setState({productos: Producto.getProductos()})
    }

    render(){
        // let productos = Producto.getProductoById(4);
       
        let filas = this.state.productos.map( el =>
            <tbody>
                <tr key={el.id}>
                    <td>{el.id}</td>
                    <td>{el.nombre}</td>
                    <td><Button onClick={()=>this.borra(el.id)}>Borra</Button></td>
                </tr>
            </tbody>    
            );
    
        return (
            <>
                <h1>Lista de productos...</h1>
                <br />
                <Table>
                    <tbody>
                    <tr>
                        <th>id</th
                        ><th>Producto</th>
                    </tr>
                    </tbody>
                    {filas}
                </Table>

                <Input onChange={this.handleInputChange} type="text" name="nombreProducto"  />
                <Button onClick={this.productoNuevo}>Añadir</Button>
            </>
           
        )
    }
  
}

export default Pantalla1;