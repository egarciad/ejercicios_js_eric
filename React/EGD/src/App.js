import React from "react";


import Navbar from "./Navbar";
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import {
  Container
} from 'reactstrap';

import Contact from "./Contact";
import Bicing from "./Bicing";
import Tiempo from "./Tiempo";
import Error404 from "./Error404";
import Bicing2 from "./Bicing2";
import Pantalla1 from "./Pantalla1";

class App extends React.Component{

  constructor(props) {
    super(props);

    this.state = {
      textoPantalla: "Selecciona" // valor inicializado, después se cambiará dinámicamente
    }

    this.cambia = this.cambia.bind(this);
  }

  cambia(nuevoTexto){
    this.setState({textoPantalla: nuevoTexto});
  }

  render() {

    return (

      <>
      
      <BrowserRouter> 
      <NavbarSite/>
        <Container>

           

          <Switch> 
                <Route exact path="/" component={Contact} /> 
                <Route path="/Contact" component={Contact} />
                <Route path="/Bicing" component={Bicing} />
                <Route path="/Bicing2" component={Bicing2} />
                <Route path="/Tiempo" component={Tiempo} />
                <Route path="/Pantalla1" component={Pantalla1} />
                <Route component={Error404} />

            </Switch>

          </Container>
        </BrowserRouter>
      </>
    );


  }

}

export default App;