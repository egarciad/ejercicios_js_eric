import React from 'react';
import { Button, Table, Container, Input, Row, Col } from 'reactstrap';

//const API_URL = "https://api.citybik.es/v2/networks/bicing";
//const API_URL = "https://www.el-tiempo.net/api/json/v2/provincias/08/municipios/08019";
//const API_URL = "https://www.el-tiempo.net/api/json/v1/provincias/08/municipios/08019/weather"
const API_URL = "https://www.el-tiempo.net/api/json/v2/provincias"

class Tiempo extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            estaciones:[],
           
        }
       
        this.carga = this.carga.bind(this);
    }
    
   
    carga() {
        
        fetch(API_URL)
            .then(response => response.json())
            .then(data => data.provincias)
            .then(estaciones => this.setState({estaciones: estaciones })) // => console.log(estaciones[0].name))
            .catch(error => console.log("no vaaaaaaaaaaaaaaa"));
        
        console.log("Fetch iniciado");
     
    }

    render() {

       // let lista = this.state.estaciones.map(el => <li>{el.name}</li>); muestra la lista de todos los elementos que hay en este caso de "name" es como un foreach

        // let lista = this.state.estaciones.map(el => <li>{el.name}</li>);  
   
         let lista = this.state.estaciones.map((el, index) => (
            
             <li key={index} >{el.CAPITAL_PROVINCIA}</li> 
         )
         );
       
        return (
            <>
                <Container>
                    <br />
                    <h2>Bicing</h2>
                    <br />
                    <Button onClick={this.carga} >Carga Bicis</Button>

                    {lista}


                </Container>
            </>
        );
    }

}



export default Tiempo;