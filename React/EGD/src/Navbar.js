import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Button,
  Form,
  Input, 
  InputGroupAddon,
  InputGroup
} from 'reactstrap';

import {Link} from 'react-router-dom';

import "./css/navbar.css";
import { FaSearch } from "react-icons/fa"; //importamos logo Buscar


const logo = require("./images/Logo_EGD.png"); // creamos constante con el logo de la web

const NavBar = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      
        <Navbar color="dark"  light expand="md">
          <Link to="/"  ><img className="logo-img" src={logo} /></Link> 
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto" navbar>
            
                <Link id="link" className="nav-link" to="/components">About Me</Link> 
                    
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle id="link" className="text-white" nav caret>
                  Resume
                </DropdownToggle>
                <DropdownMenu right>
                    <Link className="dropdown-item option" to="/components"  >Competencias</Link> 
                    <Link className="dropdown-item option" to="/components"  >Lenguajes</Link> 
                  <DropdownItem divider />
                    <Link className="dropdown-item option" to="/components"  >Resumen</Link>    
                </DropdownMenu>
              </UncontrolledDropdown>          
                <Link id="link" className="nav-link" to="/Pantalla1"  >Productos</Link>
                <Link id="link" className="nav-link" to="/Contact"  >Contact</Link>
                <Link id="link" className="nav-link" to="/Bicing"  >Api-Bicing</Link>
                <Link id="link" className="nav-link" to="/Bicing2"  >Api-Bicing2</Link>
                <Link id="link" className="nav-link" to="/Tiempo"  >Api-Tiempo-Barcelona</Link>
            </Nav>
            <Form inline>
              <InputGroup>
                <Input type="text" name="BuscarInput" id="buscarInput" placeholder="Buscar"/>
                  <InputGroupAddon addonType="append">
                    <Button type="submit" id="Buscar" ><FaSearch/></Button>
                  </InputGroupAddon>
              </InputGroup>
            </Form>
          </Collapse>
        </Navbar>



      
    </div>
    
  );
}

export default NavBar;