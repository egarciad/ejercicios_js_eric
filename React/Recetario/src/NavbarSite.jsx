import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  Button,
  Form,
  Input, 
  InputGroupAddon,
  InputGroup
} from 'reactstrap';

import {Link} from 'react-router-dom';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import "./css/navbar.css";
import Error404 from "./Error404";
import Inicio from "./Inicio";
import Productos from "./Productos";
import Recetas from "./Recetas"; 
import { FaSearch } from "react-icons/fa"; //importamos logo Buscar


const logo = require("./images/Logo_Recetario6.png"); // creamos constante con el logo de la web

const NavbarSite = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      
        <Navbar color="info"  light expand="md">
          <Link to="/"  ><img className="logo-img" src={logo} /></Link> 
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto" navbar>
                <Link id="link" className="nav-link" to="/">Inicio</Link>
                <Link id="link" className="nav-link" to="/Productos">Productos</Link>
                <Link id="link" className="nav-link" to="/Recetas">Recetas</Link>
            </Nav>
            <Form inline>
              <InputGroup>
                <Input type="text" name="BuscarInput" id="buscarInput" placeholder="Buscar"/>
                  <InputGroupAddon addonType="append">
                    <Button type="submit" id="Buscar" ><FaSearch/></Button>
                  </InputGroupAddon>
              </InputGroup>
            </Form>
          </Collapse>
        </Navbar>
    


      
    </div>
    
  );
}

export default NavbarSite;