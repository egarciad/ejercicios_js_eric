import React from "react";
import "./css/inicio.css";

import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Container, Row, Col
  } from 'reactstrap';

  import {Link} from 'react-router-dom';

  const PRODUCTOS = require("./images/Productos1.jpg"); // creamos constante con la imagen de Productos
  const RECETAS = require("./images/Recetas1.jpg"); // creamos constante con la imagen de Productos

class Inicio extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <>

            <Container>

                <Row>
                    <Col className="mt-2">
                        <h1 className="nav-link">Recetario</h1>
                    </Col>
                </Row>
                <Row>
                    <Col md="6" className="mt-3">
                    <Link to="/ListaProductos" style={{textDecoration: 'none'}} className="nav-link">
                        <Card outline color="secondary">
                            <CardImg top width="100%" className="image" src={PRODUCTOS} alt="Card image cap" />
                            <CardBody>
                            <CardTitle style={{color: 'black', fontWeight: 'bold', fontSize: '20px'}}>Productos</CardTitle>
                            <CardText style={{color: 'black', fontSize: '18px'}}>Busca por productos para encontrar recetas</CardText>
                            </CardBody>
                        </Card>
                    </Link>
                    </Col>

                    <Col md="6" className="mt-3"> 
                    <Link to="/ListaRecetas" style={{textDecoration: 'none'}} className="nav-link">
                        <Card outline color="secondary">
                            <CardImg top width="100%" className="image" src={RECETAS} alt="Card image cap" />
                            <CardBody>
                            <CardTitle style={{color: 'black', fontWeight: 'bold', fontSize: '20px'}}>Recetas</CardTitle>
                            <CardText style={{color: 'black', fontSize: '18px'}}>¡Busca las mejores recetas!</CardText>
                            {this.props.name}
                            </CardBody>
                        </Card>
                    </Link>
                    </Col>
                </Row>

                

            </Container>


            </>
        );
    }
}

export default Inicio;