import React from 'react';
import Producto from './modelos/Producto';
import { Table, Button, Input, Col, Row } from 'reactstrap';



class ListaProductos extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            productos: Producto.getProductos(),
            nombreProducto: ""
        }
        this.productoNuevo = this.productoNuevo.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.borra = this.borra.bind(this);
    }


    handleInputChange(evento) {
        const target = evento.target;
        //const value = (target.type === 'checkbox') ? target.checked : target.value;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }


    productoNuevo() {
        Producto.addProducto(this.state.nombreProducto);
        this.setState({ 
                productos: Producto.getProductos(),
                nombreProducto:"" 
            })
    }

    borra(id) {
        Producto.eliminaProducto(id * 1);
        this.setState({ productos: Producto.getProductos() })
    }

    render() {
        // let productos = Producto.getProductoById(4);

        let filas = this.state.productos.map(el =>
            <tbody>
            <tr key={el.id}>
                <td>{el.id}</td>
                <td>{el.nombre}</td>
                <td><Button onClick={() => this.borra(el.id)}>Borra</Button></td>
            </tr>
            </tbody>
        );

        return (
            <>
            <Container>
                <Row>
                    <Col>
                        <h2>Productos</h2>
                        <br />
                    </Col>
                </Row>
                <Row>
                    <Col xs="4">
                        <h4>Nuevo producto:</h4>
                            <Input 
                                value={this.state.nombreProducto} 
                                onChange={this.handleInputChange} 
                                name="nombreProducto" 
                                type="text" 
                                />
                            <br />
                            <Button disabled={this.state.nombreProducto.length<2} onClick={this.productoNuevo}>Añadir</Button>
                    </Col>
                    <Col >
                        <Table>
                            <tbody>
                            <tr>
                                <th>id</th>
                                <th>Producto</th>
                                <td></td>
                            </tr>
                            </tbody>
                            {filas}
                        </Table>
                    </Col>
                </Row>
            </Container>
            </>

        )
    }

}

export default ListaProductos;