let RECETAS = [
    {id: 1, nombre: "Pastel", descripcion: "poner en el horno y esperar"},
    {id: 2, nombre: "Huevos fritos", descripcion: "poner los huevos en la sarten hasta que esten hechos"},
];

class Receta {

    static getRecetas = () => {
        //conectar a bdd
        //esperar listado 
        // return listado;
        return RECETAS;
    } 

    static getRecetaById = (idReceta) => {
        let elReceta = RECETAS.find(el => el.id===idReceta);
        return elReceta;
    } 

    static addReceta = (nombreReceta, descripcionReceta) => {
        let max=0;
        RECETAS.forEach(el => {
            max = max<el.id ? el.id : max;
        })
        let nuevoId = max +1;
        let recetaNueva = {id: nuevoId, nombre: nombreReceta, 
            descripcion: descripcionReceta};
        RECETAS.push(recetaNueva);
    }

    static eliminaReceta = (idBorrar) => {
        RECETAS = RECETAS.filter(el => el.id!==idBorrar)
    }

}

export default Receta;