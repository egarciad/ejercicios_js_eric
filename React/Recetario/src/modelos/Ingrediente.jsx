let INGREDIENTES = [
    { id: 1, idReceta: 1, idProducto: 1, cantidad: 2, unidades: "cucharadas" }
];


class Ingrediente {


    static getIngredientes = (idRecetaPedida) => {
        return INGREDIENTES.filter(el => el.idReceta===idRecetaPedida);
    } 


    static addIngrediente = ({idReceta, idProducto, unidades, cantidad}) => {
        let max = 0;
        INGREDIENTES.forEach(el => {
            max = (max < el.id) ? el.id : max;
        })
        let nuevoId = max + 1;
        let ingredienteNuevo = {
            id: nuevoId,
            idReceta: idReceta,
            idProducto: idProducto,
            cantidad: cantidad,
            unidades: unidades
        };

        INGREDIENTES.push(ingredienteNuevo);
    }
}

export default Ingrediente;
