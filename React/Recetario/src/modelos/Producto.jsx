
/*
let PRODUCTOS = [
    {id: 1, nombre: "Harina"},
    {id: 2, nombre: "Azúcar"},
    {id: 3, nombre: "Aceite"},
    {id: 4, nombre: "Levadura"},
    {id: 5, nombre: "Sal"},
    {id: 6, nombre: "Leche"},
];
*/
let CLAVE = "recetario_productos";

class Producto {
  

    static guardar(lista){
        localStorage.setItem(CLAVE, JSON.stringify(lista));
    }

    static recuperar(){
        let productos = JSON.parse( localStorage.getItem(CLAVE) );
        if (productos!==null){
            return productos;
        }else{
            return [];
        }
    }


    static getProductos = () => {
   
        // leer de local storage
        return this.recuperar()
    } 

    static getProductoById = (idProducto) => {
        //leer lista de local storage y guardar en PRODUCTOS
        let PRODUCTOS = this.recuperar();

        let elProducto = PRODUCTOS.find(el => el.id===idProducto);
        return elProducto;
    } 

    static addProducto = (nombreProducto) => {
        let PRODUCTOS = this.recuperar();
        let max=0;
        PRODUCTOS.forEach(el => {
            max = (max<el.id) ? el.id : max;
        })
        let nuevoId = max +1;
        let productoNuevo = {id: nuevoId, nombre: nombreProducto};
        PRODUCTOS.push(productoNuevo);
        this.guardar(PRODUCTOS);

    }

    static eliminaProducto = (idBorrar) => {
        let PRODUCTOS = this.recuperar();
        PRODUCTOS = PRODUCTOS.filter(el => el.id!==idBorrar);
        this.guardar(PRODUCTOS);
    }

}

export default Producto;