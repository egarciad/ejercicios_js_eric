import React, { useState } from 'react';
import {Container, Collapse, Navbar, NavbarToggler, Nav, Button, Form, Input, InputGroupAddon, InputGroup} from 'reactstrap';
import {BrowserRouter, Switch, Route, Link} from 'react-router-dom';

import { FaSearch } from "react-icons/fa"; //importamos logo Buscar
import "./css/navbar.css";

import Error404 from "./Error404";
import Inicio from "./Inicio";
import ListaProductos from "./ListaProductos";
import ListaRecetas from "./ListaRecetas"; 
import EditaReceta from "./EditaReceta"; 


const logo = require("./images/Logo_Recetario6.png"); // creamos constante con el logo de la web

const Navegacion = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
        <Navbar color="info"  light expand="md">
                <Link to="/"  ><img className="logo-img" src={logo} /></Link> 
          <NavbarToggler onClick={toggle} />  
          <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto" navbar>
                <Link id="link" className="nav-link" to="/">Inicio</Link>
                <Link id="link" className="nav-link" to="/ListaProductos">Productos</Link>
                <Link id="link" className="nav-link" to="/ListaRecetas">Recetas</Link>
            </Nav>
            <Form inline>
              <InputGroup>
                    <Input type="text" name="BuscarInput" id="buscarInput" placeholder="Buscar"/>
                  <InputGroupAddon addonType="append">
                    <Button type="submit" id="Buscar" ><FaSearch/></Button>
                  </InputGroupAddon>
              </InputGroup>
            </Form>
          </Collapse>
        </Navbar>
    </div>
    
  );
}


    export default function App(){
    return(
    <>
      <BrowserRouter> 
        <Navegacion/>
        <Container>
          


          <Switch> 
                <Route exact path="/" component={Inicio} /> 
                <Route path="/ListaProductos" component={ListaProductos} />
                <Route path="/ListaRecetas" component={ListaRecetas} /> 
                <Route path="/EditaReceta/:idReceta" component={EditaReceta} /> 
                <Route component={Error404} />

          </Switch>

        </Container>
      </BrowserRouter>
    </>
  );
}


